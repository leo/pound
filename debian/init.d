#!/bin/sh
# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          pound
# Required-Start:    $local_fs $syslog
# Required-Stop:     $local_fs $syslog
# Should-Start:      $named
# Should-Stop:       $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: reverse proxy and load balancer
# Description:       reverse proxy, load balancer and
#                    HTTPS front-end for Web servers
### END INIT INFO

DAEMON=/usr/sbin/pound
DESC="reverse proxy and load balancer"
NAME=pound
PIDFILE=/run/pound.pid

do_start_prepare() {
    if [ ! -f "/etc/pound.cfg" ]; then
	log_daemon_msg "Pound unconfigured, not (re)starting (/etc/pound.cfg is missing)\n"
	exit 0
    elif $DAEMON -c ; then
	true
    else
	log_daemon_msg "Failed to validate config file, refusing to (re)start\n"
	exit 0
    fi
}

do_restart_prepare() {
    do_start_prepare
}
